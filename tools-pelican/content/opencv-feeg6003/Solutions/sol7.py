"""Task 7: Scaling with tx=100 ty=50"""
import numpy as np
import cv2 as cv

"""Read an image"""
img = cv.imread('logo.png')

"""Construct parameters"""
rows, cols = img.shape[:2]
M = np.float32([[1,0,100],[0,1,50]]) 

"""Pass M to cv.warpAffine"""
image_new = cv.warpAffine(img,M,(cols,rows))

"""Display"""
cv.imshow('Translation',image_new)
cv.waitKey(0)
cv.destroyAllWindows()

