import cv2
import numpy as nump

#Load an image in color
colorimg = cv2.imread('atlantis.jpg',1)

#Load an image in grayscale
grayimg = cv2.imread('atlantis.jpg',0)

# Show the colored image aforeloaded
cv2.imshow('Color', colorimg)
# Show the grayscale image aforeloaded
cv2.imshow('Gray', grayimg)

# To write a modified image
cv2.imwrite('atlantisgray.jpg', grayimg)

# Wait for a key stroke
cv2.waitKey(0)
# After the key stroke destroy all windows
cv2.destroyAllWindows()