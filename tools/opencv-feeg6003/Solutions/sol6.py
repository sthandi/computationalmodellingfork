"""Task 6: Scaling with fx=1.5 fy=1.5"""
import numpy as np 
import cv2 as cv 

"""Read an image"""
img = cv.imread('logo.png',1) 

"""Scaling using cv.resize()"""
res = cv.resize(img, None, fx=1.5, fy=1.5, interpolation=cv.INTER_CUBIC)

"""Disaplay"""
cv.imshow('Scaling',res) 
cv.waitKey(0) 
cv.destroyAllWindows()

